apiVersion: templates.gatekeeper.sh/v1beta1
kind: ConstraintTemplate
metadata:
  name: cerntrustedregistry
  annotations:
    description: Requires container images to begin with a registry string from a specified list.
spec:
  crd:
    spec:
      names:
        kind: CERNTrustedRegistry
      validation:
        openAPIV3Schema:
          type: object
          properties:
            registries:
              type: array
              items:
                type: string
  targets:
    - target: admission.k8s.gatekeeper.sh
      rego: |
        package cerntrustedregistry
        violation[{"msg": msg}] {
          container := input.review.object.spec.containers[_]
          satisfied := [good | registry = input.parameters.registries[_] ; good = startswith(container.image, registry)]
          not any(satisfied)
          msg := sprintf("container <%v> has an invalid image registry <%v>, allowed registries are %v", [container.name, container.image, input.parameters.registries])
        }
        violation[{"msg": msg}] {
          container := input.review.object.spec.initContainers[_]
          satisfied := [good | registry = input.parameters.registries[_] ; good = startswith(container.image, registry)]
          not any(satisfied)
          msg := sprintf("CERN policy mandates usage of a trusted registry - currently %v. Container <%v> has an invalid image registry <%v>", [input.parameters.registries, container.name, container.image])
        }
---
apiVersion: templates.gatekeeper.sh/v1beta1
kind: ConstraintTemplate
metadata:
  name: cernrequiredlabels
spec:
  crd:
    spec:
      names:
        kind: CERNRequiredLabels
      validation:
        openAPIV3Schema:
          properties:
            labels:
              type: array
              items: string
  targets:
    - target: admission.k8s.gatekeeper.sh
      rego: |
        package cernrequiredlabels
        violation[{"msg": msg, "details": {"missing_labels": missing}}] {
          provided := {label | input.review.object.metadata.labels[label]}
          required := {label | label := input.parameters.labels[_]}
          missing := required - provided
          count(missing) > 0
          msg := sprintf("CERN policy mandates a minimum set of labels on workloads, currently %v. Missing labels: %v", [required, missing])
        }
